# Sys Service Catalogue

The sys service catalogue contains all major roles for providing services. In the future this repo will be splitted in multiple repos.

## Docs

This repository contains docs concerning some major workflow inside the **docs** repository.

Direct access to the documentation [SYSTEM SQUAD OFFICIAL DOCUMENTATION](https://digit-c4.pages.code.europa.eu/sys-service-catalogue/)

## Contributing

Before contributing please see the [CONTRIBUTING](CONTRIBUTING.md)
