# Git workflow

New changes **MUST** be done in a separated branch with a dedicated Merge Request.

# Ansible Playbooks

Ansible playbooks **MUST** contain a vim line at the end **# vim: ft=yaml.ansible**.

All roles **MUST** have a test in tests/ that can be used to validate the role.

# Merge Requests

Merge Requests **MUST** be reviewed before being merged.

Merge requests **SHOULD** rebase their branch on `main` before merging to avoid merge conflicts.

# Versionning

New versions **MUST** be created via a git tag of the following format: `v$MAJOR.$MINOR.$PATCH`.

Version numbers **MUST** follow Semantic Versionning.

# Architecture Decision Records

Merge Requests act as ADR.

An ADR is a record of "why" things are done in a certain way. This is important
for onboarding new people on the project as they can just go through the
history of ADR to understand the project.

