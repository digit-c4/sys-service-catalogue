# All service primitives in OpenAPI format

You will find all the service primitives offered by the SYS squad in this page, in OpenAPI format.

<iframe src="../_static/openapi.html" width="100%" height="1000px" style="border:none;"></iframe>