# Linux Reference Configuration

## Use case
The debian12 image is a minimal image with only the necessary packages installed, the image is hardened and is compliant with the company security standards.

## Technical description

### Breaking Changes:
- No Ldap Authentication 
- No debian mirroring
- Only docker services are allowed
- Resolvconf migrated to systemd-resolv
- Ntpd migrated to systemd-timesyncd
- Images are build with packer this means a few softwares are preinstalled like docker, systemd-timesyncd...
- Different root and snet password per machine and stored to vault

### How to get a new package?
If you want a specific binary installed on the machine, you will need to run it as a docker container. No new packages can be added to the image by the user themselves, except you ask the system team and they validate the demande.

### Reference config
Packages installed:
- docker
- rsync
- rsyslog
- netbox-docker-agent (container which runs by default)
- systemd-timesyncd
- systemd-resolved

Resolvconf has been replaced by systemd-resolved.

Chronyd has been replaced by systemd-timesyncd.

A standard user "snet" with the group "snmc" exist by default on the virtual machine, no other user can be created.

The rsyslog localy on the machine has been configured to accept logs in /var/run/snet_log.sock and those logs will are forwarde by design to the centralized rsyslog server.

The default rootca are by default stored inside the os.


## Roadmap
- [ ] Remove snet from sudoers
- [ ] Disallow ssh connection
