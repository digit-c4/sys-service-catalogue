# Create VM

## Use case
The virtualization primitive is used to spawn a virtual machine in various places with the same configuration. Everybody who wants to create a virtual machine **must** use this primitive otherwise no access to the virtualization platform will be granted. 

## Technical description
### Specs
Providers:
- Vmware
- AWS

Machine sizing Vmware:
- small (2vcpu, 8gb ram, 50gb disk)
- medium (4vcpu, 16gb ram, 50gb disk)
- large (8vcpu, 32gb ram, 50gb disk)

Machine sizing AWS:
- small (2vcpu, 8gb ram, 50gb disk) (t2.large)
- medium (4vcpu, 16gb ram, 50gb disk) (t2.Xlarge)
- large (8vcpu, 32gb ram, 50gb disk) (t2.2Xlarge)

Those virtual machine will have by design a single network interface (More interfaces can be added trough another service, while the machine is up and running). 

The disk space is by default 50gb. Any additional space, should be requested and will be added if needed.

The OS is by default Debian 12 (see: [Debian12](./../definition/Debian12) ).
### Request a new virtual machine
To request a new virtual machine, the user **MUST** provide the following information see: [Openapi Virtualization](./../definition/OpenApi):
```yaml
# Example VmWare:
data:
  providerType: vmware
  regionCluster: testRegionCluster
  platform: debian12
  name: testMachine
  vmware: 
    datastore: testDataStore
  esxi_hostname: testEsxi
  machineType: small
  network:
    dpg: testDPG
    gateway: 192.168.0.1
    ip_address: 192.168.0.10
    netmask: 255.255.255.0
    domain_name: example.com
  dependencies:
    dns_server: testdns.example.com
    ntp_server: testntp.example.com

# Example AWS:
data:
  providerType: aws
  regionCluster: eu-west-1
  platform: debian12
  name: testMachine
  machineType: small
  aws: 
    ssh_key: testKey
    assignPublicIp: false
    subnetId: testsubnetId
    securityGroup: testSecurityGroup
  # Currently the network part is managed trough dhcp
```

## Implementation
Today the playbook is run by the System Squad:
```bash
ansible-playbook create-vm-vworker.yml 
```

This will deploy the playbook in the provided virtualization platform with the provided data.

### Ansible
The playbook is written in ansible and is using the following roles:
- ec.sys.create-vm ([ec.sys.create_vm](https://code.europa.eu/digit-c4/sys-ansible-collection/-/tree/main/roles/create_vm?ref_type=heads))

This role do include all necesseray tasks based on the providerType.

## Roadmap
- [ ] Add more providers
- [ ] Add more machine types
- [ ] Provide the netbox plugin for machine creation

