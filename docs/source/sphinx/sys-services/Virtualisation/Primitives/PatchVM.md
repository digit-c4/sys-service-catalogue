# Patch VM
## Description
What

    This project provides snapshot/patch/reboot/snmp ack for Debian VMs provided by SYS for a given bubble using that bubble AWX.

    This project DOES NOT check the machine services. For that, see the monitoring service instead.
    
When

    The patching workflow is executed each Sunday morning using AWX schedules per service, device_roles or tags.

How

![AWX patching workflow](docs/patching_sequence_diagram.png)

    After a period of three days, that snapshot is deleted using another dedicated AWX schedule.

    One can also delete snapshot using the related playbook by setting var
    snapshot_date: "YYYY-MM-DD" in the template extra vars.


## Requirements
AWX FW rules

    The bubble AWX is in FW object 'NS-SYS-AWX-ALL';
    see SNOW ticket INC02544739 for a request example.

AWX Inventory

    The target VM to patch is defined in sys_service_catalogue_inventory with at least one the following Netbox object defined:
    - device_roles  # ex: --limit device_roles_sys
    - service       # ex: --limit service_netbox
    - tags          # ex: --limit tags_netbox-docker-agent


## AWX credentials
The following credential are required to access the V, retrieve the provider secret and and send snmp ack to Smarts.

machine credential

    The following AWX credential is required to ssh on target VM:
    (SYS_snet_ssh is specific to each bubble)

    | Provider        | AWX credential |
    ------------------|-----------------
    | VMware/on prem  | SYS_snet_ssh   |
    | AWS             | aws-snet       |

custom credential hashicorp 'SYS_vcenter_lookup' (one approle per bubble)

    VAULT_ROLE_ID
    VAULT_SECRET_ID
    VAULT_NAMESPACE 

custom credential 'SYS_smarts_community' (common accross bubbles)

    snmp_community

## Playbooks usage

### ansible/playbooks/patch.yml:
extra vars:

    ---
    # no variable required

### ansible/playbooks/snmp_ack.yml
extra vars:

    ---
    receiver_target:
      - 10.133.3.200
      - 10.133.3.220
    enterprise_oid: 1.3.6.1.4.1.99999.37

### ansible/playbooks/(take|delete)_vmware_snapshot.yml
extra vars:

    ---
    patching_hyperv_hostname: vcenter-dub.snmc.cec.eu.int
    patching:
      hyperv_guest_name: dub.tech
      snapshot_vault_path: vmware/data/central/vcenter-dub.cec.eu.int/sys/vcenter-api


Since SYS VM are renamed in .ec.local in the Netbox, the vcenter guest VM is obtained based on the ansible_fqdn and variable 'hyperv_guest_name'.

Only the complete vcenter guest name and first part of the ansible_fqdn string pattern must match.



example:

        |-------------------------|-----------------------------|
        | {{ ansible_fqdn }}      |  vfoo.bar.tech.ec.europa.eu |
        |-------------------------|-----------------------------|
        | vcenter guest name      |  vfoo.bar.tech              |
        |-------------------------|-----------------------------|
        | {{ hyperv_guest_name }} |  bar.tech                   |
        |-------------------------|-----------------------------|
        | netbox VM name          |  whatever.ec.local          |
        |-------------------------|-----------------------------|


    fqdn 'vfoo.bar.tech.ec.europa.eu' and vcenter guest 'foo.bar.tech' will not match !!
