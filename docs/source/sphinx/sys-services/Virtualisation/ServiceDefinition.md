# Service definition

## Service purpose
Customers of this service receive a virtual machine (description below) with resources and network connectivity. Users can run their image (typically a virtual appliance) or request a Linux installation from the SYS squad.

Objective: enable DIGIT.C.4 teams to install and operate virtual appliances and Linux applications using a platform that is compliant the applicable rules and standards, and without having to manage virtualisation software or the underlying hardware.

## Service personas

Personas:
- Service supplier: DIGIT.C.4.SYS
- Service user:
  * DIGIT.C.4 teams
  * Other DIGIT units offer virtualisation services to users outside DIGIT.C.4.

## Service scope and limits

- A Virtual Machine provides:
  - Access to processing resources (CPU, RAM)
  - Access to storage resources local to the VM. Other storage services are available through the Storage service (link).
  - Network connectivity
    - Depends on the bubble

- 24x7 data centre support, for the components listed above 
  - SYS supports the Virtual Machine and the Linux layer. The application is supported by the user.
  - Users expected to know how to run a container.

- Support high availability services

- Optionally, Linux Reference OS configuration


- Machines are listed in Netbox.
- Name registered in DNS of the bubble
- All Virtual Machines are registered in the monitoring system and 24x7 (). Whenever a problem (what problem exactly?) is detected with a VM, the necessary actions are initiated in order to solve it. This covers the VM and the OS. To monitor the application, the user will need to configure it accordingly. See monitoring service for more information.
- Graphing
- Machines are configured to log to a logging server: see logging service
- Machines are updated regularly. Users must support this. Typically, should take 15 min., users to load-balance and schedule on different days.
- VMs must run at all times, to enable monitoring and patching.

- Access
    - Through the network only. (console-like access?)
    - Read-only access to Virtualisation components to monitor app.
    - Admin access?
    - In the future: access to VMs only through a bastion. Users to prepare for that.

## Service limits

  - Cannot rename VM. Destroy and recreate
  - >⚠️ **NOT protected by backups**

## Technical description
### Virtualisation platform

- Virtualisation platform belongs to SYS. Can be based on VMware or Nutanix. Depends on the location.

### Operating system

- Debian Linux
As of the time of writing, Debian 12.
Future: expect to migrate to newer versions

Linux OS Reference Configuration: see (link)


## Pre-requisites

### Bubble
  - Need to be in a bubble.
  
### Technical knowledge




#### What can I run?

#### Availability








### Containers
docker




### Monitoring

## Capacity planning

DIGIT.C.4.SYS plans the capacity of the service according to visible trends.
As soon as your squad can foresee a heavy use of this service, make sure to communicate your needs to the SYS squad, so that they can plan the capacity of the infrastructure.

## Lifecycle

## Non-functional requirements

- Dev? Acc? Prod?

## Availability option
To enable users to build HA environments, they can specify a "region?" when they request a VM. SYS will provide infrastructure as independent as possible in that bubble.

## Roles and responsibilities
- The SYS squad:
  - Offers primitives to create a VM, configure it, remove it, etc. Primitives are listed above in this page.
  - Maintenance of the primitives automation.
  - Monitors the VM and, if unavailable, resolve the incident.
  - Updates the OS of the VM following the policies of the unit, e.g. patches, configurations for security, implementation of best practices.
  - Coordinates planned downtime and communicates unplanned downtime.
  - Maintains the infrastructure required by the VM: 
    - For on-premises: includes the virtualisation platform and the underlying hardware. 
    - In the cloud: 
  - Manage the support contracts with the IT equipment providers
  - Capacity planning of the infrastructure


- Users of the service:
  - Call primitives exposed by the SYS team to create, configure and remove the VMs.
  - Verify the size of VMs is adequate and avoid over-consuming
  - Use the VM to run their applications (containers?)
  - Maintain the application
  - Knowledge of high availability concepts
  - Verify that the Virtualisation service can meet the needs of the application and design their solution accordingly
  - Have the necessary knowledge to use the service, e.g. scripting, process and application controls, file systems and users rights, container management
  - Provide input for capacity planning


## Service management

### Requesting the service

- Where exactly today?

### Points of contact

| Product owner | Javier |
| Proxy product owner | Fabien | 
| Incidents | ? |
| Capacity planning | 

## Future

- No ssh access
- No LDAP
- Containers only

## TO DO
Access to the VM
Which locations in the network?
Sizes