$version: "2"
namespace eu.europa.ec.snet

use aws.protocols#restJson1

@title("Sys")
@restJson1
service Sys {
    version: "1.0.0"
    resources: [
        VirtualMachine
        Interface
        RoutingTable
        Routing
        Ruling
        Sysctl
        DynamicRouting
    ]
}
