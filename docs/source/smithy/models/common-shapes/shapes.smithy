$version: "2.0"

namespace eu.europa.ec.snet

@pattern("^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$")
@length(min: 1, max: 128)
string UuidV4


@pattern("^[0-6]-((?:[01]\\d|2[0-3]):[0-5]\\d)$")
// A string representing a day of the week and a time of the day, 0 = Sunday , 1 = monday
string weekdayTime
