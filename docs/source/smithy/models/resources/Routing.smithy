$version: "2"
namespace eu.europa.ec.snet

resource Routing {
    identifiers: {
        routingId: String
    }
    properties: {
       interfaceAction: RoutingInterfaceAction
       action: RoutingAction
       gateway: String
       destination: String
       routingTableId: String
    }
    create: CreateRoute
    delete: DeleteRoute
    read: GetRoute
}

@http(method: "GET", uri: "/api/ip/route/{routingId}")
@readonly
@tags(["Routing"])
operation GetRoute {
    input := for Routing{
        @required
        @httpLabel
        $routingId
    }

    output := for Routing{
        @required
        $routingId

        @required
        $interfaceAction

        @required
        $action

        @required
        $gateway

        @required
        $destination

        @required
        $routingTableId
    }
}


@http(method: "POST", uri: "/api/ip/route", code: 201)
@tags(["Routing"])
operation CreateRoute {
    input := for Routing{
        @required
        $interfaceAction

        @required
        $action

        @required
        $gateway

        @required
        $destination

        @required
        $routingTableId
    }

    output := for Routing{
        @required
        $routingId
    }
    
}

@http(method: "DELETE", uri: "/api/ip/route/{routingId}")
@idempotent
@tags(["Routing"])
operation DeleteRoute {
    input := for Routing{
        @required
        @httpLabel
        $routingId
    }

    output := for Routing{
        @required
        $routingTableId
    }
}

enum RoutingInterfaceAction {
    UP = "up"
    DOWN = "down"
}

enum RoutingAction {
    ADD = "add"
    DELETE = "del"
}

resource Ruling {
    identifiers: {
        rulingId: String
    }
    properties: {
       interfaceAction: RoutingInterfaceAction
       action: RoutingAction
       source: String
       destination: String
       routingTableId: String
    }
    create: CreateRule
    delete: DeleteRule
    read: GetRule
}

@http(method: "GET", uri: "/api/ip/rule/{rulingId}")
@readonly
@tags(["Ruling"])
operation GetRule {
    input := for Ruling{
        @required
        @httpLabel
        $rulingId
    }

    output := for Ruling{
        @required
        $rulingId

        @required
        $interfaceAction

        @required
        $action

        @required
        $source

        @required
        $destination

        @required
        $routingTableId
    }
}

@http(method: "POST", uri: "/api/ip/rule", code: 201)
@tags(["Ruling"])
operation CreateRule {
    input := for Ruling{
        @required
        $interfaceAction

        @required
        $action

        @required
        $source

        $destination = ""

        @required
        $routingTableId
    }

    output := for Ruling{
        @required
        $rulingId
    }
    
}

@http(method: "DELETE", uri: "/api/ip/rule/{rulingId}")
@idempotent
@tags(["Ruling"])
operation DeleteRule {
    input := for Ruling{
        @required
        @httpLabel
        $rulingId
    }

    output := for Ruling{
        @required
        $routingTableId
    }
}
