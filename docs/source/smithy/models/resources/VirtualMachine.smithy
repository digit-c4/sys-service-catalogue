$version: "2"
namespace eu.europa.ec.snet

resource VirtualMachine {
    identifiers: {
        vmId: String
    }
    properties: {
        name : String
        provider_credentials : ProviderCredentials
        provider_type : ProviderType
        type : MachineType
        network : Network
        owner: String
        platform: String
        maintenance_window : weekdayTime
        dependencies: Dependencies
        aws: AwsCreate
        vmware: VmwareCreate
    }
    create: CreateVm
}

@http(method: "POST", uri: "/api/virtual-machine", code: 201)
@tags(["Provisioning"])
operation CreateVm {
    input := for VirtualMachine{
        @required
        $provider_credentials

        @required
        $provider_type
        
        $name = "Random uuidv4"

        @required
        $type

        @required
        $network

        @required
        $owner

        @required
        $platform

        $maintenance_window = "0-00:00",

        @required
        $dependencies

        $aws

        $vmware
    }

    output := for VirtualMachine{
        @required
        $name

        @required
        $vmId
    }
    
}

structure AwsCreate {
    @required
    security_group_id: String,

    @required
    vpc_id: String,

    @required
    assign_public_ip: Boolean

    @required
    ssh_key: String
}

structure VmwareCreate {
    @required
    datastore: String,

    @required
    esxi_hostname: String,

    @required
    dpg: String
}

structure Network {
    @required
    ip_address : String,

    @required
    domain_name : String,

    @required
    gateway : String,

    @required
    netmask : String,

    @required
    interface_name : String

}


structure ProviderCredentials {
    @required
    url: String,

    @required
    user: String,

    @required
    password: String,

    @required
    region: String
}

enum ProviderType{
    OVH
    VMWARE
    AWS
}

enum MachineType {
    SMALL
    MEDIUM
    LARGE
}

structure Dependencies {
    ntp_server: String,

    @required
    dns_server: String

    @required
    syslog_server: String
}
