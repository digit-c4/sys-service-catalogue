$version: "2"
namespace eu.europa.ec.snet

resource RoutingTable {
    identifiers: {
        routingTableId: String
    }
    properties: {
        interfaceId: String
        name: String
    }
    create: CreateRoutingTable
    delete: DeleteRoutingTable
    update: UpdateRoutingTable
    read: GetRoutingTable
}

@http(method: "GET", uri: "/api/routingtable/{routingTableId}")
@readonly
@tags(["RoutingTable"])
operation GetRoutingTable {
    input := for RoutingTable{
        @required
        @httpLabel
        $routingTableId
    }

    output := for RoutingTable{
        @required
        $interfaceId

        @required
        $name
    }
}


@http(method: "POST", uri: "/api/routingtable", code: 201)
@tags(["RoutingTable"])
operation CreateRoutingTable {
    input := for RoutingTable{
        @required
        $name

        @required
        $interfaceId
    }

    output := for RoutingTable{
        @required
        $routingTableId
    }
}

@http(method: "DELETE", uri: "/api/routingtable/{routingTableId}")
@idempotent
@tags(["RoutingTable"])
operation DeleteRoutingTable {
    input := for RoutingTable{
        @required
        @httpLabel
        $routingTableId
    }
}

@http(method: "PUT", uri: "/api/routingtable/{routingTableId}")
@idempotent
@tags(["RoutingTable"])
operation UpdateRoutingTable {
    input := for RoutingTable{
        @required
        @httpLabel
        $routingTableId
    }
}
