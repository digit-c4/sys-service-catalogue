$version: "2"
namespace eu.europa.ec.snet

resource Sysctl {
    identifiers: {
        sysctlId: String
    }
    properties: {
        key: String
        value: String
        vmId: String
    }
    create: CreateSysctl
    delete: DeleteSysctl
    read: GetSysctl
}

@http(method: "GET", uri: "/api/sysctl/{sysctlId}")
@readonly
@tags(["Sysctl"])
operation GetSysctl {
    input := for Sysctl{
        @required
        @httpLabel
        $sysctlId
    }

    output := for Sysctl{
        @required
        $key

        @required
        $value

        @required
        $vmId
    }
}


@http(method: "POST", uri: "/api/sysctl", code: 201)
@tags(["Sysctl"])
operation CreateSysctl {
    input := for Sysctl{
        @required
        $vmId

        @required
        $key

        @required
        $value

    }

    output := for Sysctl{
        @required
        $sysctlId
    }
    
}

@http(method: "DELETE", uri: "/api/sysctl/{sysctlId}")
@idempotent
@tags(["Sysctl"])
operation DeleteSysctl {
    input := for Sysctl{
        @required
        @httpLabel
        $sysctlId
    }
}

