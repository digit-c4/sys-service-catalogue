$version: "2"
namespace eu.europa.ec.snet

resource Interface {
    identifiers: {
        interfaceId: String
    }
    properties: {
        type: InterfaceType
        name: String
        vmId: String
    }
    create: CreateInterface
    update: UpdateInterface
    delete: DeleteInterface
    read: GetInterface
}


@http(method: "GET", uri: "/api/interface/{interfaceId}")
@readonly
@tags(["Interface"])
operation GetInterface {
    input := for Interface{
        @required
        @httpLabel
        $interfaceId
    }

    output := for Interface{
        @required
        $interfaceId

        @required
        $type

        @required
        $name

        @required
        $vmId
    }
}

@http(method: "POST", uri: "/api/interface", code: 201)
@tags(["Interface"])
operation CreateInterface {
    input := for Interface{
        $type = "physical" 

        @required
        $name

        @required
        $vmId
    }

    output := for Interface{
        @required
        $interfaceId
    }
    
}

@http(method: "PUT", uri: "/api/interface/{interfaceId}")
@idempotent
@tags(["Interface"])
operation UpdateInterface {
    input := for Interface{
        @required
        @httpLabel
        $interfaceId
    }

    output := for Interface{
        @required
        $interfaceId
    }
}

@http(method: "DELETE", uri: "/api/interface/{interfaceId}")
@idempotent
@tags(["Interface"])
operation DeleteInterface {
    input := for Interface{
        @required
        @httpLabel
        $interfaceId
    }
}

enum InterfaceType {
    VIRTUAL = "virtual"

    PHYSICAL = "physical"
}
