$version: "2"
namespace eu.europa.ec.snet

resource DynamicRouting {
    identifiers: {
        dynamicRoutingId: String
    }
    properties: {
        target_net: String
        target_routes: target_routes
        vmId: String
    }
    create: CreateDynamicRouting
    delete: DeleteDynamicRouting
    read: GetDynamicRouting
}

list target_routes {
    member: String
}

@http(method: "GET", uri: "/api/dynamic_routing/{dynamicRoutingId}")
@readonly
@tags(["DynamicRouting"])
operation GetDynamicRouting {
    input := for DynamicRouting{
        @required
        @httpLabel
        $dynamicRoutingId
    }

    output := for DynamicRouting{
        @required
        $target_net

        @required
        $target_routes

        @required
        $vmId
    }
}


@http(method: "POST", uri: "/api/dynamic_routing", code: 201)
@tags(["DynamicRouting"])
operation CreateDynamicRouting {
    input := for DynamicRouting{
        @required
        $vmId

        @required
        $target_net

        @required
        $target_routes

    }

    output := for DynamicRouting{
        @required
        $dynamicRoutingId
    }
    
}

@http(method: "DELETE", uri: "/api/dynamic_routing/{dynamicRoutingId}")
@idempotent
@tags(["DynamicRouting"])
operation DeleteDynamicRouting {
    input := for DynamicRouting{
        @required
        @httpLabel
        $dynamicRoutingId
    }
}

