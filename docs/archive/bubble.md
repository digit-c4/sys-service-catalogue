# Bubble (Decentralized management Platform)
 
### Schema
![Bubble](diagram_generator/output/LV_OVH.png)

## Use case
A bubble should be a mini Infrastructure which we deploy'd aside an application.
Example:
```
You need to manage firewalls. And for each pair of firewalls you have a different subnet, wich is used to manage this pair of firewalls. 

Old Way (Centralized Way):
    - A pair of machines have access to all firewals 
    - One single source of truth

    Pro:
        - Easy to visualize, since you have only on entry point
        - Easy to audit and manage access 

    Contra:
        - Impossible to provide access to the client, since all clients use the same way 
        - One impact on the management system, you cannot access any firewall 
        - One security impact, will impact all firewalls 
        - One management system maintenenace, will impact each firewall
        - To do a large change, you need to keep down the whole management system 

New Way (Decentralized Way):
    - Multiple machines in each subnet and they only have access inside the subnet and not outside
    - A source of truth per managmenet subnet 

    Pro:
        - You can grand different access levels for each subnet without the fear of a dataleak
        - If a maangement system has some issues, you can still manage the other firewalls 
        - If there is a dataleak on one management system, the other firewalls will not be impacted 
        - You can do a lot of small migration steps, instead of migrating the whole management stack 

    Contra:
        - You need to standarize the process
        - Monitoring will be based per each Decentralized maangement platform
       
```

### Inside the bubble
Inside the bubble, each squad will provide different solutions:

System Sqaud:
- Syslog
- Ntp
- Awx
- Bastion Host
- Prometheus / Grafana / Alertmanager

CMDB Squad:
- Netbox

Reverse Proxy Sqaud:
- Nginx

DNS Squad:
- Bind9

Undefined Squad:
- HashiCorp Vault

### Configuration Setup
Each service provide by the system teams should have a "minimum Working configuration".

This means, that our virtual machines should already be inside netbox. That the prometheus shoudl already monitor the system machines and send alerts if they're down and have some visual metrics stored inside grafana. All the machines should always send the logs to the local syslog and have the ntp server set to the local ntp server.

Inside AWX, there should already be our custom playbooks, which are used to manage the bubble.

All Web applications should be behind the Reverse Proxy, provided by the Reverse Proxy Squad.
