# Netbox

Netbox is the source of truth of the sys catalogues and also the ipam solution.

## Data Model
Each service, that we deploy should contain a service template inside netbox (https://demo.netbox.dev/static/docs/core-functionality/services/) and each machine who host the specific service, should be mappedto the service template.

Example:
* you have a prometheus, then you should have a service template prometheus
* a service should be created based on the service template and mapped to the machine who host the service template 

Each machine with the associated ip address should be stored inside netbox, together with the ip range of the bubble.

The following data are also needed:
* Platform > debian12
* Platform > esxi 
* Platform > vcenter
* Device Roles > SYS 
* EC/DC 
    * Cluster Type > Vmware
    * Cluster > Vmware 1 
    * Device Types > add the types of the esxi hosts
    * Devices > All ESXi Host 
* Cloud 
    * Cluster Type > Cloud 
    * Cluster > Cloud 1
* Netbox DNS (To check with the dns team for the view configuration and co)
    * Netbox Dns Records > All virtual Machines should have the record set here 
* Mappings > Add mapping for netbox 
* Mappings > Add mapping for awx 
* Mappings > Add mapping for prometheus


## Update/Upgrade
The netbox instance, should be upgraded every day at 2 am. This task should be a job template inside awx with a shedule. (To be checked with the owner of netbox).


