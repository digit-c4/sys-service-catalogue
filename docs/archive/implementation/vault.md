# Sys Vault Path

If we need to share the creds across all squads, the role is : all

{techno}/data/{bubble name}/{os}/{FQDN}/{role}/user
```
Example: linux/testing/debian/example.com/sys/test-user
    - user 
    - password

AppRoles:
    Virtual Machine Creation:
        Path:
            linux/data/testing/debian/+/+/+:
                - Creation/Update
    Virtual Machine Upgrade/Update:
        Path:
            linux/data/testing/debian/+/+/test-user:
                - Lecture

    RA - Machine access:
        Path:
            linux/data/+/debian/+/ra/+:
                - Lecture


{techno}/data/{bubble name}/{os}/{FQDN}/{role}/user
Example: vmware/data/testing/esxi/esxi.example.com/sys/test-user
         vmware/data/testing/vcenter/vcenter.example.com/sys/test-user
    Virtual Machine Creation:
        Path:
            vmware/data/testing/vcenter/vcenter.example.com/sys/test-user:
                - Lecture
```
## How to use 
Currently this setup is working only for debian12 and will not be maintened for debian11.
To be able to consum the password from vault to connect to a virtual machine, multiple variables need to be set:

```
vault_vars:
  environment: ENVIRONMENT
  bubble: BUBBLE

os_users:
  USER1:
    role: all
  USER2:
    role: sys

ansible_become_password: >-
  {{ lookup('community.hashi_vault.hashi_vault',
  'secret=linux/data/{{ vault_vars.bubble }}/debian/{{ inventory_hostname }}/{{ os_users.snet.role | default("sys") }}/test-user:password
  auth_method=approle
  role_id={{ vault[vault_vars.environment]["all"]["APPROLE"]["role_id"] }} 
  url={{ vault[vault_vars.environment]["all"]["APPROLE"]["vault_server"] }} 
  secret_id={{ vault[vault_vars.environment]["all"]["APPROLE"]["secret_id"] }}
  namespace={{ vault[vault_vars.environment]["all"]["APPROLE"]["namespace"] }} 
  validate_certs=False') }}

ansible_ssh_password: >-
  {{ lookup('community.hashi_vault.hashi_vault',
  'secret=linux/data/{{ vault_vars.bubble }}/debian/{{ inventory_hostname }}/{{ os_users.snet.role | default("sys") }}/test-user:password
  auth_method=approle
  role_id={{ vault[vault_vars.environment]["all"]["APPROLE"]["role_id"] }} 
  url={{ vault[vault_vars.environment]["all"]["APPROLE"]["vault_server"] }} 
  secret_id={{ vault[vault_vars.environment]["all"]["APPROLE"]["secret_id"] }}
  namespace={{ vault[vault_vars.environment]["all"]["APPROLE"]["namespace"] }} 
  validate_certs=False') }}

vault:
  ENVIRONMENT:
    BUBBLE:
      APPROLE:
        vault_server: VAULT_SERVER
        role_id: ROLE_ID
        secret_id: SECRET_ID
        namespace: NAMESPACE
```


