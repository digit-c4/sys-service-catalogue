from diagrams import Diagram
from diagrams.c4 import Person, Container, Database, System, Relationship, SystemBoundary

graph_attr = {
    "splines": "spline",
}

with Diagram("Machine Creation diagram", direction="TB", filename="output/machineCreation", show=False, graph_attr=graph_attr):
    customer = Person(
        name="SNET User", description="A customer of the sys service for vm deployment"
    )

    netbox = Container(
        name="Netbox",
        technology="Netbox on docker",
        description="The digit C4 source of thruth"
    )

    with SystemBoundary("AWX Workflow"):
        awx = Container(
            name="AWX",
            technology="AWX on docker",
            description="The ansible runner"
        )

        gitlab = System(
            name="Gitlab",
            technology="Version Control Software",
            description="The version control software which hosts the playbooks"
        )

        hashicorp_vault = Database(
            name="Hashicorp Vault",
            technology="Hashicorp Vault",
            description="Credential storage"
        )

    virtualization_center = System(
        name="virtualization",
        technology="Vmware / AWS / OVH",
        description="Virtualization center", 
        external=True
    )

    virtual_machine = System(
        name="Virtual Machine",
        technology="Debian12 / VApp",
        description="A virtual machine",
        external=True
    )

    customer >> Relationship("Visits Netbox and create a new virtual Machine") >> netbox
    netbox >> Relationship("Starts a webhook call to awx") >> awx
    awx >> Relationship("Downloads playbooks from gitlab") >> gitlab
    awx >> Relationship("Retrieve secrets from vault") >> hashicorp_vault
    awx >> Relationship("Deploy vm on virtualization_center") >> virtualization_center
    awx >> Relationship("Connects to virtual machine to deploy some configuration") >> virtual_machine
