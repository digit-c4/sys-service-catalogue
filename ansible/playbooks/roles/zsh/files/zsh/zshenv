#!/bin/zsh
# /etc/zshenv: system-wide .zshenv file for zsh(1).
#
# This file is sourced on all invocations of the shell.
# If the -f flag is present or if the NO_RCS option is
# set within this file, all other initialization files
# are skipped.
#
# This file should contain commands to set the command
# search path, plus other important environment variables.
# This file should not contain commands that produce
# output or assume the shell is attached to a tty.
#
# Global Order: zshenv, zprofile, zshrc (zaliases, zhosts, zusers), zlogin


# ==================== PATH ======================
PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/X11R6/bin"
MANPATH=/usr/man/:/usr/local/man/:/usr/share/man/:/usr/X11R6/man/

if [ -d /opt/csw/ ]; then
  if [ -d /opt/csw/bin ]; then
    PATH="${PATH}:/opt/csw/bin"
  fi
  if [ -d /opt/csw/sbin ]; then
    PATH="${PATH}:/opt/csw/sbin"
  fi
  if [ -d /opt/csw/man ]; then
    MANPATH="${MANPATH}:/opt/csw/man"
  fi
fi
if [ -d ${HOME}/bin ]; then 
    PATH="${PATH}:${HOME}/bin"
fi
if [ -d /opt/cxoffice/bin ]; then 
    PATH="${PATH}:/opt/cxoffice/bin"
fi
if [ -d /cygdrive/d/Program\ Files/PuTTY ]; then 
    PATH="${PATH}:/cygdrive/d/Program Files/PuTTY"
fi

export PATH

# Man pages
export -U MANPATH

if [[ -o rcs ]];
then

  # path to minimal functions
  fpath=("/etc/zsh/zbin" "/etc/zsh/functions" "$HOME/.zsh/zbin" $fpath)
  autoload +X /etc/zsh/zbin/*(:t)
  if [ -d $HOME/.zsh/zbin ]; then
    autoload +X $HOME/.zsh/zbin/*(:t)
  fi

  # add directories to fpath here with fpath_autoload:
  # this will add each directory and autoload all
  # functions within
  fpath_autoload "/etc/zsh/zbin"
  fpath_autoload "$HOME/.zsh/zbin"

fi

# ==================== LANG ======================
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export LANGUAGE="en_US"
#export LC_MESSAGES=en_US
#export GDM_LANG=en_US.UTF-8

#export LANG=fr_FR@euro.ISO-8859-15
#export LANGUAGE=fr_FR
#export LC_MESSAGES=fr_FR
#export LC_ALL=fr_FR@euro
#export GDM_LANG=fr_FR@euro.ISO-8859-15
#zshrc_load_status locales environment

# =================== HISTORY ====================
[[ -d ~/.zsh/history ]] || mkdir -p ~/.zsh/history/ || HISTFILE=~/.zsh_history
[[ -d ~/.zsh/history ]] && HISTFILE=~/.zsh/history/zsh_history${HOST:+"_${HOST}"}

export HISTSIZE=100000
export SAVEHIST=100000
# If we do not export SAVEHIST, history is not saved
DIRSTACKSIZE=10

# ===================== CVS ======================
export CVSROOT=""
export CVS_RSH=ssh
export CVSEDITOR=vi


# ==================== TERM ======================
if [ "$TERM" = "rxvt" ] ; then
    export TERM=xterm
fi

if [ "$TERM" = "xterm" ]; then
    unset TMOUT
#else
#    export TMOUT=15
fi


# ==================== MISC ======================

export EMAIL_ADDR=test

# Default editor is emacs
export EDITOR="vim"

# Wordchars
# don't treat '/' as part of a word, e.g. M-Backspace etc. will only
# delete up to the path seperator '/'. the default setting is
# *?_-.[]~=/&;!#$%^(){}<>
export WORDCHARS="*?_-.[]~=&;!#$%^(){}<>"

# in order to scroll if completion list is too big
export LISTPROMPT

# export that libraries already loaded
export INIT="true"

if [ -d /usr/share/R/share/perl ]; then
	export PERL5LIB=$PERL5LIB:/usr/share/R/share/perl:.
fi

# load the intel compiler
#  SUPERDOME
if [ -f /opt/intel/cc/9.1.042/bin/iccvars.sh ] ; then
	source /opt/intel/cc/9.1.042/bin/iccvars.sh
fi
#
if [ -f /opt/intel/idb/9.1.042/bin/idbvars.sh ] ; then
	source /opt/intel/idb/9.1.042/bin/idbvars.sh
fi
#
# local ZIV last version
if [ -f /opt/intel/cc/10.0.025/bin/iccvars.sh ] ; then
	source /opt/intel/cc/10.0.025/bin/iccvars.sh
fi
#
if [ -f /opt/intel/idb/10.0.025/bin/idbvars.sh ] ; then
	source /opt/intel/idb/10.0.025/bin/idbvars.sh
fi

# mails
unset MAILCHECK
#export MAIL=/var/spool/mail/$USER
export MAIL=~/Maildir
export MAILDIR=${MAIL}
#zshrc_load_status set spool for mails
