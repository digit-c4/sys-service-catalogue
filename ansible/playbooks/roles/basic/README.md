# group_vars/foo.yml example:
-----------
Dedicated group var example:

  ---
  vault_approle: sys-awx-debian-install-foo
  owner: sys
  
  vault_vars:
    environment: production
    bubble: foo
  
  os_users:
    user1:
      role: sys
    root:
      role: sys


# inventory
-----------
Update inventory to include your host_var/bar.fqdn.yml file and corresponding group_var
(the group_var debian12 is encrypted and contains the vault definition)
example:

  ---
  [debian12:children]
  foo
  
  [foo]
  bar.fqdn


# playbook execution
-----------
ansible-playbook playbooks/basic.yml --limit bar.fqdn --ask-vault-pass -kK -u user1

