# Description:

This role is to add the docker hosts in Netbox.
If there are already some hosts it will delete them and add all the hosts that it finds in Nebtox in the Virtualization/Virtual Machines section.

## How to run the role

Run the Role using Ansible AWX.
Create a template and add the required Variables.
Add a limit to netbox.ec.local so that it will only run on that vm.
Then run the playbook.

### Variables needed

netbox_docker_url: "http://netbox.ec.local:8080/api/plugins/docker/hosts/"
netbox_token: "Token ##NETBOX_TOKEN##"
base_url: "http://netbox.ec.local:8080"
host_endpoint: "http://"
user: "##AGENT_USERNAME##"
password: "##AGENT_PASSWORD##"
port: "##AGENT_PORT##"
