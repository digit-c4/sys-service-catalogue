# How to add Alerts

To add alerts, open the rules.yml file in the files folder.
Once you have opened this file, you can add the new rules under the last one.

## Example:

```

  - name: Basic Alerts
    rules:

    - alert: InstanceDown
      expr: up == 0
      for: 5m
      labels:
        severity: critical
      annotations:
        summary: Instance {{ $labels.instance }} is down
        description: The instance {{ $labels.instance }} has been down for more than 5 minutes.

```

If you add your alert under this example, it will be displayed under the Basic alerts group.
To create a new group, you only need to add the following

```

  - name: new group
    rules:

```

before you add the alert.

# Enable the Alerts

To activate the alerts, go to awx and re-launch the Prometheus installation.
This will copy the newly created alerts and restart the Prometheus server so that the alerts are activated.

# Config

To send the alerts to the syslog server, we need the Alert Manager, as Prometheus cannot send the alerts directly to the syslog server.

To configure prometheus to send the alerts to the Alert Manager, add this to your prometheus configuration.

```

alerting:
  alertmanagers:
    - static_configs:
        - targets: ['{{ alertmanager }}']

```

Add this to the prometheus configuration as well. This causes the rules.yml file to be used as the rules file for prometheus.

```

rule_files:
  - /etc/prometheus/rules.yml

```
