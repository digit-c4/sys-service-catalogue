# Description

This role is to configure openobserve.

## Variables needed:

authorization: "email:password"
openobserve_url: "url of openobserve"
data_retention: "how long the data should be keept"
function_body: >
  {
    "function": "\nif exists(.program) {\n   if contains(string!(.program), \"nginxrps\") {\n       . |= parse_logfmt!(.message)\n   }\n\n   if contains(string!(.program), \"ansible-community.general.syslogger\") {\n       . |= parse_logfmt!(.message)\n   }\n}\n",
    "name": "default"
  }
