# Alert Manager Role

This role installs the Alert Manager. We need the Alert Manager to manage alerts sent by Prometheus. The Alert Manager allows us to transmit our alerts to Syslog, for example.

# Configure to foward alerts

To allow the alert manager to send the alerts, we need a receiver and receivers where we add the url where the alert manager should send the alerts.
These variables can be defined in Ansible AWX in the extra vars.

```
Example when the alertmanager should not send any data:

receiver: "null-receiver" #null-receiver
receivers:
  - name: 'null-receiver'

Example:

receivers:
  - name: logstash
    webhook_configs:
    - url: "http://127.0.0.1"
      send_resolved: true
    - url: "http://localhost"
```
