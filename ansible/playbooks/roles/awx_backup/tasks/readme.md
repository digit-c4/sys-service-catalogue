The aim of this role is to dump the postgres database and to send it to a remote backup server

Variables summary

| Variable                    | Description                                                        |
|-----------------------------|--------------------------------------------------------------------|
| `postgres.container`        | Name of the PostgreSQL container                                   |
| `postgres.password`         | Password for connecting to PostgreSQL                              |
| `postgres.database`         | Name of the PostgreSQL database                                    |
| `postgres.port`             | Port for connecting to PostgreSQL                                  |
| `postgres.user`             | Username for connecting to PostgreSQL                              |
| `dump.base_path`            | Base path for saving the dump locally                              |
| `archive_server`            | Archive server for storing backups                                 |
| `backup_user`               | User on the backup server                                          |
| `scp_password`              | Password for the SCP connection                                    |
