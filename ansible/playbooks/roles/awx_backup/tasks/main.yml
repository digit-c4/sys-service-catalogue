---

- name: Check rights of folder above backup folder
  ansible.builtin.file:
    path: /opt/SNet
    mode: ug+x

- name: Create backup directory
  ansible.builtin.file:
    path: "{{ dump.base_path }}"
    state: directory
    mode: '0755'

- name: Folder mount point
  ansible.builtin.command:
    cmd: "df -P {{ dump.base_path }}"
  register: df_output
  changed_when: false

- name: Extract mount point from df_output
  ansible.builtin.set_fact:
    mount_point: "{{ df_output.stdout_lines[-1].split()[-1] }}"

- name: Print mount_point var
  ansible.builtin.debug:
    var: mount_point

- name: Get info on filesystem
  ansible.builtin.setup:
    filter: 'ansible_mounts'
  register: mount_info

- name: Get info on the mountpoint
  ansible.builtin.set_fact:
    mount_data: "{{ mount_info.ansible_facts.ansible_mounts | selectattr('mount', 'equalto', mount_point) | first }}"

- name: Calculate free space on mountpoint
  ansible.builtin.set_fact:
    mount_free_space_mb: "{{ (mount_data.size_available / 1024 / 1024) | int }}"

- name: Debug free space on mountpoint
  ansible.builtin.debug:
    msg: "Free space on {{ mount_point }}: {{ mount_free_space_mb }} Mo"

- name: Get awx DB size
  community.docker.docker_container_exec:
    container: tools_postgres_1
    command: psql -U '{{ postgres.user }}' -t -c "SELECT (pg_database_size('awx') / 1024 / 1024)::integer AS size_mb;"
  register: db_size_result

- name: Set DB size as a fact
  ansible.builtin.set_fact:
    db_size_mb: "{{ db_size_result.stdout | trim | int }}"

- name: Display DB size
  ansible.builtin.debug:
    msg: "The size of the 'awx' DB is {{ db_size_mb }} MB"

- name: Check if there's enough space for the backup
  ansible.builtin.assert:
    that:
      - "mount_free_space_mb | int > (db_size_mb | int * 4)"
    fail_msg: "Not enough space on disk. Free space: {{ mount_free_space_mb }} MB, Required: {{ (db_size_mb | int * 4) }} MB"
    success_msg: "Enough space available for backup"

- name: Backup awx
  community.docker.docker_container_exec:
    container: "{{ postgres.container }}"
    env:
      PG_PASSWORD: "{{ postgres.password }}"
    argv:
      - 'pg_dump'
      - '-d'
      - "{{ postgres.database }}"
      - '-h'
      - '127.0.0.1'
      - '-p'
      - '{{ postgres.port }}'
      - '-U'
      - '{{ postgres.user }}'
      - '-c'
      - '-C'
      - '-f'
      - '/tmp/postgresql_dump.sql'

- name: Copy backup to localhost
  ansible.builtin.command:
    cmd: "docker cp {{ postgres.container }}:/tmp/postgresql_dump.sql {{ dump.base_path }}/{{ ansible_fqdn }}_{{ ansible_date_time.iso8601_basic_short }}.sql"
  changed_when: false

- name: Remove dump from container
  community.docker.docker_container_exec:
    container: "{{ postgres.container }}"
    command: "rm /tmp/postgresql_dump.sql"

- name: "Find backup files on {{ ansible_fqdn }}"
  ansible.builtin.find:
    paths: "{{ dump.base_path }}/"
    patterns: "{{ ansible_fqdn }}_*.sql"
  register: find_result

- name: "Check if backup files was found"
  ansible.builtin.fail:
    msg: "ERROR - No backup files found to retrieve"
  when: "find_result.matched < 1"

- name: 'Archive backup file(s) before transfer'
  community.general.archive:
    path: "{{ item.path }}"
    dest: "{{ item.path }}.tar.gz"
    mode: '644'
  loop: "{{ find_result.files }}"

- name: Install sshpass
  ansible.builtin.package:
    name: sshpass
  become: true

- name: "Copy compressed backup file on {{ archive_server }}"
  ansible.builtin.shell: >
    sshpass -p "{{ scp_password }}" scp
    -o StrictHostKeyChecking=no
    -o UserKnownHostsFile=/dev/null
    -o KexAlgorithms=+diffie-hellman-group1-sha1
    -o Ciphers=+3des-cbc
    -o HostKeyAlgorithms=+ssh-rsa
    -o PubkeyAcceptedKeyTypes=+ssh-rsa
    "{{ item.path }}.tar.gz"
    "{{ backup_user }}"@"{{ archive_server }}:/{{ item.path | basename }}.tar.gz"
  loop: "{{ find_result.files }}"
  register: scp_result
  changed_when: "'changed' in scp_result.stdout"

- name: "Clean tmp file(s) on {{ ansible_fqdn }}"
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: "absent"
  loop: "{{ find_result.files }}"

- name: "Clean compressed tmp file(s) on {{ ansible_fqdn }}"
  ansible.builtin.file:
    path: "{{ item.path }}.tar.gz"
    state: "absent"
  loop: "{{ find_result.files }}"
