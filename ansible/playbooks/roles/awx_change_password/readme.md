AWX Token and Password Management with Vault
This Ansible playbook manages AWX tokens and passwords using HashiCorp Vault. It ensures secure handling of credentials while maintaining flexibility in token management. A password rotation is to occur every month on AWX. This job will be part in a bundle of templates for AWX

**Overview**
    The playbook performs the following key operations:
1. Retrieves existing token from Vault (if present)
2. Creates a new token if none exists
3. Generates a new random password
4. Updates Vault with the latest credentials
5. Updates the AWX user password

**Prerequisites**

Ansible with community.hashi_vault and awx.awx collections installed
Access to a HashiCorp Vault instance
AWX/Tower instance

**Playbook Structure**

1. Read Current Token from Vault

This task retrieves the current token and other secret data from Vault. It uses AppRole authentication and specifies the path where the AWX secrets are stored.

2. Create Token if Not Present

This task creates a new AWX token only if no token was found in Vault. It uses either the password from Vault or a default password.

3. Set Token Fact

This task sets the awx_token variable to either the existing token from Vault or the newly created token.

4. Generate Random Password

A new random password is generated each time the playbook runs.

5. Write AWX Data to Vault

This task updates Vault with the latest AWX credentials, including the username, new password, and token.

6. Set AWX User Password

Finally, this task updates the AWX user's password with the newly generated password.

**Usage**

To use this playbook:
Ensure all required variables are defined in your inventory or vars file:

- Hashicorp_vault.url
- Hashicorp_vault.namespace
- Hashicorp_vault.bubble
- vault_approle_id
- vault_approle_secret_id
- engine_mount_point
- default_username
- default_password
